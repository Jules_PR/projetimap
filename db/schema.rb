# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_09_09_184355) do

  create_table "achauffers", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "label"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "adresses", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "servsmtp"
    t.string "password"
    t.integer "mailCount", default: 0
    t.date "beginDate"
    t.integer "age", default: 0
    t.integer "hourlyLimit", default: 20
    t.integer "sendHour"
    t.integer "nextHour"
    t.integer "port"
    t.string "authentication"
    t.string "starttls"
    t.integer "nbHours", default: 0
    t.boolean "isValid", default: false
    t.string "domain"
  end

  create_table "imaps", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "label"
    t.string "login"
    t.string "password"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "repCount"
    t.integer "inboxCount"
    t.integer "spamCount"
    t.integer "importantCount"
    t.integer "mailCount", default: 0
    t.string "isActive", default: "active"
    t.boolean "isValid"
    t.string "servimap"
  end

end
