class AddPortEtauthenticationEtstarttlsToAdresses < ActiveRecord::Migration[6.1]
  def change
    add_column :adresses, :port, :integer
    add_column :adresses, :authentication, :string
    add_column :adresses, :starttls, :string
  end
end
