class AddMailCountToAdress < ActiveRecord::Migration[6.1]
  def change
    add_column :adresses, :mailCount, :integer
  end
end
