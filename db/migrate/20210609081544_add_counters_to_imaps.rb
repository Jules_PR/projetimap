class AddCountersToImaps < ActiveRecord::Migration[6.1]
  def change
    add_column :imaps, :inboxCount, :integer
    add_column :imaps, :spamCount, :integer
    add_column :imaps, :importantCount, :integer
  end
end
