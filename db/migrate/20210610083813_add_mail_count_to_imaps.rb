class AddMailCountToImaps < ActiveRecord::Migration[6.1]
  def change
    add_column :imaps, :mailCount, :integer, :default => 0
  end
end
