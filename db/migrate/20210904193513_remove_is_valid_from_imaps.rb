class RemoveIsValidFromImaps < ActiveRecord::Migration[6.1]
  def change
    remove_column :imaps, :isValid, :string
  end
end
