class AddDefaultToMailCount < ActiveRecord::Migration[6.1]
  def change
    change_column :adresses, :mailCount, :integer, default: 0
  end
end
