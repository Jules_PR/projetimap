class CreateImaps < ActiveRecord::Migration[6.1]
  def change
    create_table :imaps do |t|
      t.string :label
      t.string :servimap
      t.string :servsmtp
      t.string :login
      t.string :password

      t.timestamps
    end
  end
end
