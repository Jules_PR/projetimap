class AddHourlyLimitToAdresses < ActiveRecord::Migration[6.1]
  def change
    add_column :adresses, :hourlyLimit, :integer, default: 20
  end
end
