class AddNbhoursToAdresses < ActiveRecord::Migration[6.1]
  def change
    add_column :adresses, :nbHours, :integer, default: 0
  end
end
