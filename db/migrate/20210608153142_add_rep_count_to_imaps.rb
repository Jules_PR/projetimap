class AddRepCountToImaps < ActiveRecord::Migration[6.1]
  def change
    add_column :imaps, :repCount, :integer
  end
end
