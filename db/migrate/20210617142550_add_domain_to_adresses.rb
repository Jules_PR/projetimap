class AddDomainToAdresses < ActiveRecord::Migration[6.1]
  def change
    add_column :adresses, :domain, :string
  end
end
