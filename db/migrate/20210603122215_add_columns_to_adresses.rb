class AddColumnsToAdresses < ActiveRecord::Migration[6.1]
  def change
    add_column :adresses, :servsmtp, :string
    add_column :adresses, :password, :string
  end
end
