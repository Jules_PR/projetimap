class AddAgeToAdresses < ActiveRecord::Migration[6.1]
  def change
    add_column :adresses, :age, :integer, default: 0
  end
end
