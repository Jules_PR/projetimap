class RemoveServFromImaps < ActiveRecord::Migration[6.1]
  def change
    remove_column :imaps, :servsmtp, :string
    remove_column :imaps, :servimap, :string
  end
end
