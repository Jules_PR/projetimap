class AddBeginDateToAdresses < ActiveRecord::Migration[6.1]
  def change
    add_column :adresses, :beginDate, :date
  end
end
