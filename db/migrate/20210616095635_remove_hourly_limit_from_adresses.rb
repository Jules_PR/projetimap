class RemoveHourlyLimitFromAdresses < ActiveRecord::Migration[6.1]
  def change
    remove_column :adresses, :hourlyLimit, :integer
  end
end
