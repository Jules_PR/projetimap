class RemoveHoursFromAdresses < ActiveRecord::Migration[6.1]
  def change
    remove_column :adresses, :sendHour, :integer, :default =>  Date.today.hour
    remove_column :adresses, :nextHour, :integer, :default => Date.today.hour
  end
end
