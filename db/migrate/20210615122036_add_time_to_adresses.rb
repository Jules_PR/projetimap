class AddTimeToAdresses < ActiveRecord::Migration[6.1]
  def change
    add_column :adresses, :sendHour, :integer
    add_column :adresses, :nextHour, :integer
  end
end
