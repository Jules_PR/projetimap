class RemoveNbMailsEtDomainFromAdresses < ActiveRecord::Migration[6.1]
  def change
    remove_column :adresses, :domain, :string
    remove_column :adresses, :nbMails, :string
  end
end
