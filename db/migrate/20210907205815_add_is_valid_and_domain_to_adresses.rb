class AddIsValidAndDomainToAdresses < ActiveRecord::Migration[6.1]
  def change
    add_column :adresses, :isValid, :boolean, default: :false
    add_column :adresses, :domain, :string
  end
end
