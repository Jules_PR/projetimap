class AddReTimeToAdresses < ActiveRecord::Migration[6.1]
  def change
    add_column :adresses, :nextHour, :integer, default: 0
  end
end
