class AddIsValidToImaps < ActiveRecord::Migration[6.1]
  def change
    add_column :imaps, :isValid, :boolean
  end
end
