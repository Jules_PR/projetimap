class RemoveLabelFromAdresses < ActiveRecord::Migration[6.1]
  def change
    remove_column :adresses, :label, :string
  end
end
