class AddIsActiveToImaps < ActiveRecord::Migration[6.1]
  def change
    add_column :imaps, :isActive, :string, default: "active"
  end
end
