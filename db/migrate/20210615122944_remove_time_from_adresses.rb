class RemoveTimeFromAdresses < ActiveRecord::Migration[6.1]
  def change
    remove_column :adresses, :nextHour, :integer
  end
end
