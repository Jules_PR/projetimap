Rails.application.routes.draw do
  root 'homepage#index'
  get '/homepage/help', to: 'homepage#help'
  
  resources:imaps
  resources:adresses
end