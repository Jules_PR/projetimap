class ApplicationMailer < ActionMailer::Base
  default from: 'julespr66test@gmail.com'
  layout 'mailer'
end
