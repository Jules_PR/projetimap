class ReponseMailer < ApplicationMailer
    def new_reponse_email(from, address, message_id, options)
        this_message_id = "<#{SecureRandom.uuid}111@ubuntu.mail>"
        mail(to: address, reply_to:from, from: from, subject: "Re: reponse", body: "J ai bien recu le mail", references: message_id, in_reply_to: message_id, message_id: this_message_id, delivery_method_options: options)
    end
end
#references: "#{message_id}",