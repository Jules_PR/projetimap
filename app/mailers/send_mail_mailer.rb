require 'faker'
require 'net/smtp'

class SendMailMailer < ApplicationMailer
    def new_mail(fromAdress, toImap, options)
        mail(to: toImap, from: fromAdress, subject: Faker::Name.name, body: Faker::Company.bs, delivery_method_options: options)
    end
end