require 'net/imap'

class AdressesController < ApplicationController
  def index
    @adresses = Adress.order(:isValid).reverse
    @imaps = Imap.all
    @adresses.each do |adresse|
      #met à jour "l'age" des adresses
      if adresse.nbHours == 24
        adresse.update(age: adresse.age + 1)
        adresse.update(nbHours: 0)
      end
      #augmente de 30% la limite de mails par heure en fonction de l'ancienneté (20 par defaut au premier jour)
      if adresse.age == 0
        adresse.update(hourlyLimit: 20)
      else
        adresse.update(hourlyLimit: 20)
        for i in 1..adresse.age
          adresse.update(hourlyLimit: adresse.hourlyLimit + (adresse.hourlyLimit * 0.3).round(0).to_i)
        end
      end
    end
    CheckValidityJob.perform_later()
  end

  def show
    @adress = Adress.find(params[:id])
    @activeImaps = Imap.where("isValid = true").where("isActive = 'active'")

    if @adress.mailCount == nil
      @adress.update(mailCount: 0)
    end
    SendMailJob.perform_later(params[:id])
  end

  def new
    @adress = Adress.new
  end

  def create
    @adress = Adress.new(adress_params)

    if @adress.save
      @adress.update(beginDate: Date.today)
      begin
        smtp = Net::SMTP.start(adress.servsmtp, adress.port, adress.domain, adress.email, adress.password)
        @adress.update!(isValid: true)
      rescue
        #reste sur la meme page si il y a eu une erreur de connexion au serveur mail
        @adress.update!(isValid: false)
        redirect_to edit_adress_path(@adress)
      ensure
        if smtp != nil
          smtp.finish
        end
      end
      if @adress.isValid
        redirect_to '/adresses'
      end
    else
      render :new #renvoie a new si il y a eu un problème
    end
  end

  def edit
    @adress = Adress.find(params[:id])
    @activeImaps = Imap.where("isValid = true").where("isActive = 'active'")
  end

  def update
    @adress = Adress.find(params[:id])

    if @adress.update(adress_params)
      begin
        smtp = Net::SMTP.start(adress.servsmtp, adress.port, adress.domain, adress.email, adress.password)
        @adress.update!(isValid: true)
      rescue
        #reste sur la meme page si il y a eu une erreur de connexion au serveur mail
        @adress.update!(isValid: false)
        redirect_to edit_adress_path(@adress)
      ensure
        if smtp != nil
          smtp.finish
        end
      end
      if @adress.isValid
        redirect_to '/adresses'
      end
    else
      render :edit #renvoie a edit si il y a eu un problème
    end
  end

  def destroy
    @adress = Adress.find(params[:id])
    @adress.destroy
    redirect_to '/adresses'
  end

  private
    def adress_params
      params.require(:adress).permit(:servsmtp, :email, :password, :mailCount, :beginDate, :age, :port, :starttls, :authentication, :isValid, :domain)
    end
end
