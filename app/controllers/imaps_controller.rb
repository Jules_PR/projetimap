require 'net/imap'
require 'resolv-replace'

class ImapsController < ApplicationController

  def index
    @imaps = Imap.all
  end
  
  def show
    @imap = Imap.find(params[:id])
    @imapAccess = nil
    @adresses = Adress.all
  end

  def new
    @imap = Imap.new
  end

  def create
    @imap = Imap.new(imap_params)

    if @imap.save
      #test si l'adresse créée est valide, renvoie sur le menu d'édition si elle ne l'est pas
      begin
        imapAccess = Net::IMAP.new(@imap.servimap, 993, true)
        imapAccess.login(@imap.login, @imap.password)
        @imap.update!(isValid: true)
      rescue
        @imap.update!(isValid: false)
        redirect_to edit_imap_path(@imap)
      ensure
        if imapAccess != nil
          imapAccess.disconnect
        end
      end
      if @imap.isValid
        redirect_to '/adresses'
      end
    else
      render :new #renvoie a new si il y a eu un problème
    end
  end

  def edit
    @imap = Imap.find(params[:id])
  end

  def update
    @imap = Imap.find(params[:id])

    if @imap.update(imap_params)
      #test si l'adresse éditée est valide,  renvoie sur le menu d'édition si elle ne l'est pas
      begin
        imapAccess = Net::IMAP.new(@imap.servimap, 993, true)
        imapAccess.login(@imap.login, @imap.password)
        @imap.update!(isValid: true)
      rescue
        @imap.update!(isValid: false)
        redirect_to edit_imap_path(@imap)
      ensure
        if imapAccess != nil
          imapAccess.disconnect
        end
      end
      if @imap.isValid
        redirect_to '/adresses'
      end
    else
      render :edit #renvoie a edit si il y a eu un problème
    end
  end

  def destroy
    @imap = Imap.find(params[:id])
    @imap.destroy
    redirect_to '/adresses'
  end

  private
    def imap_params
      params.require(:imap).permit(:label, :servimap, :servsmtp,:login, :password, :isActive, :isValid)
    end
end