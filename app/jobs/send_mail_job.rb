require 'net/smtp'

class SendMailJob < ApplicationJob
  queue_as :default
  sidekiq_options retry: false

  def perform(id)
        
    imaps = Imap.all
    activeImaps = []

    imaps.each do |imap|
      if imap.isActive == 'active' && imap.isValid
        activeImaps.push(imap)
      end
    end

    adress = Adress.find(id)

    error = false

    #définit les paramètres SMTP
    delivery_options = 
    {
        port: adress.port,
        user_name: adress.email,
        password: adress.password,
        address: adress.servsmtp,
        authentication: adress.authentication,
        enable_starttls_auto: ActiveModel::Type::Boolean.new.cast(adress.starttls), #convertit en boolean le string 'true' ou 'false' stocké dans adress.starttls
    }

    #update l'heure actuelle
    adress.update(sendHour: Time.now.hour)
    if adress.nextHour == nil
      adress.update(nextHour: Time.now.hour)
    elsif (adress.nextHour - adress.sendHour) > 1
      adress.update(nextHour: adress.sendHour)
    end

    #met à jour "l'age" des adresses
    if adress.nbHours == 24
      adress.update(age: adress.age + 1)
      adress.update(nbHours: 0)
    end
    
    #augmente de 30% la limite de mails par heure en fonction de l'ancienneté (20 par defaut au premier jour)
    if adress.age == 0
      adress.update(hourlyLimit: 20)
    else
      adress.update(hourlyLimit: 20)
      for i in 1..adress.age
        adress.update(hourlyLimit: adress.hourlyLimit + (adress.hourlyLimit * 0.3).round(0).to_i)
      end
    end
    
    #fixe la derniere limite a 300 000 de mails par heures
    if adress.hourlyLimit >= 300000
      adress.update(hourlyLimit: 300000)
    end

    #session d'envoie
    if adress.isValid
      if adress.sendHour >= adress.nextHour
        adress.update(nextHour: Time.now.advance(hours: 1).hour)
        adress.update(nbHours: adress.nbHours + 1)

        sent = 1
        debut = "#{Time.now.hour}:#{Time.now.min}"
        puts "-----DEBUT DE L'ENVOI DES #{adress.hourlyLimit * activeImaps.length} MAILS DE #{adress.email} (début à : #{debut}, fin estimée dans 30min)-----".yellow
        activeImaps.each do |imap|
          #limite le nombre d'envois de mail en fonction de l'heure actuelle et de la limite par heure
          n = 0
          for i in 1..adress.hourlyLimit
            n += 1
            unless error
              adress.increment!(:mailCount)
              SendMailMailer.new_mail(adress.email, imap.login, delivery_options).deliver_now
              puts "[#{Time.now.hour}:#{Time.now.min}] #{adress.email} : Mail envoyé #{sent}/#{adress.hourlyLimit * activeImaps.length}".blue
              #test les adresses tous les 10 mails
              if n == 10
                begin
                  smtp = Net::SMTP.start(adress.servsmtp, adress.port, adress.domain, adress.email, adress.password)
                rescue
                  puts "[#{Time.now.hour}:#{Time.now.min}] #{adress.email} : ERREUR LORS DE L'ENVOIE, FIN DE LA SESSION".red
                  #retire l'heure du compteur d'heures d'activité de l'adresse
                  adress.update(nbHours: adress.nbHours - 1)
                  error = true
                ensure
                  if smtp != nil
                    smtp.finish
                  end
                end
                n = 1
              end
              sent += 1
              #répartit l'envoie des mails sur une période de 30min
              sleep 1800/adress.hourlyLimit/activeImaps.length
            else
              next
            end
          end
          error = false
        end
        puts "-----FIN DE L'ENVOI DES #{adress.hourlyLimit * activeImaps.length} MAILS DE #{adress.email} (début à : #{debut}, fin à : #{Time.now.hour}:#{Time.now.min})-----".yellow
        sent = 1
      else
        puts "La session de l'heure de cette adresse à deja été faite ou est en cours".red
      end
    else
      puts "L'adresse n'a pas pu se connecter".red
    end
    puts "-----DEBUT DES REPONSES-----".green
    activeImaps.each do |imap|
      ImapJob.perform_later(imap.id)
    end
  end
end