require 'net/imap'

class CheckValidityJob < ApplicationJob
  queue_as :default

#test les adresses afin de les passer en "invalide" si elles ne parviennent pas à se connecter aux serv mail
  def perform()
    adresses = Adress.all
    puts "------- DEBUT DU TEST DES ADRESSES -------".yellow
    adresses.each do |adress|
      begin
        smtp = Net::SMTP.start(adress.servsmtp, adress.port, adress.domain, adress.email, adress.password)
        adress.update!(isValid: true)
        puts "L'adresse à chauffer #{adress.email} fonctionne correctement".green
      rescue
        adress.update!(isValid: false)
        puts "L'adresse à chauffer #{adress.email} ne parvient pas à se connecter".red
      ensure
        if smtp != nil
          smtp.finish
        end
      end
    end

    imaps = Imap.all
    imaps.each do |imap|
      begin
        imapAccess = Net::IMAP.new(imap.servimap, 993, true)
        imapAccess.login(imap.login, imap.password)
        imap.update!(isValid: true)
        puts "L'adresse réceptrice #{imap.login} fonctionne correctement".green
      rescue => e
        puts e
        imap.update!(isValid: false)
        puts "L'adresse #{imap.login} ne parvient pas à se connecter".red
      ensure
        if imapAccess != nil
          imapAccess.disconnect
        end
      end
    end
    puts "------- FIN DU TEST DES ADRESSES -------".yellow
  end
end
