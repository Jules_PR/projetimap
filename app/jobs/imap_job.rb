require 'net/imap'

class ImapJob < ApplicationJob
  queue_as :default
  sidekiq_options retry: false

  def perform(id)
    imap = Imap.find(id)
    adresses = Adress.all

    #définition des dossier IMPORTANT et Spam pour les différentes boites mail ainsi que les paramètres SMTP
    spamFolder = ""
    importantFolder = ""
    servimap = nil
    case imap.label
    when "Gmail"
        spamFolder = "[#{imap.label}]/Spam"
        importantFolder = "Starred"

        #paramètres SMTP pour action mailer
        delivery_options = { port: 587,
          user_name: imap.login,
          password: imap.password,
          address: 'smtp.gmail.com',
          enable_starttls_auto: true,
          authentication: 'plain' }

    when "Yahoo"
        spamFolder = "Bulk Mail"
        importantFolder = "Important"

        delivery_options = { port: 587,
          user_name: imap.login,
          password: imap.password,
          address: 'smtp.mail.yahoo.com',
          authentication: 'plain',
          enable_starttls_auto: false
        }

    when "AOL"
        spamFolder = "Bulk Mail"
        importantFolder = "Important"

        delivery_options = { port: 587,
          user_name: imap.login,
          password: imap.password,
          address: 'smtp.aol.com',
          authentication: 'plain',
          enable_starttls_auto: false
          }

    when "Outlook"
        spamFolder = "Junk"
        importantFolder = "Important"

        delivery_options = { port: 587,
          domain: 'outlook.com',
          user_name: imap.login,
          password: imap.password,
          address: 'smtp.office365.com',
          enable_starttls_auto: true,
          authentication: 'login' }

    else
        spamFolder = "[#{imap.label}]/Spam"
        importantFolder = "Important"

        delivery_options = { port: 587,
          user_name: imap.login,
          password: imap.password,
          address: imap.servsmtp,
          enable_starttls_auto: true,
          authentication: 'plain' }
    end

    #connection en IMAP
    imapAccess = Net::IMAP.new(imap.servimap, 993, true)
    imapAccess.login(imap.login, imap.password)

      begin
      #créé un dossier IMPORTANT si la boite mail n'en a pas
      if not imapAccess.list("[#{imap.label}]/", importantFolder)
        imapAccess.create("[#{imap.label}]/#{importantFolder}")
      end
      
      #parcourt la boite de reception, déplace les mails dans IMPORTANT et répond aved un mail
      imapAccess.select('INBOX')
      adresses.each do |email|
        imapAccess.search(["FROM", email.email]).each do |messageFrom|
          envelopeArray = imapAccess.fetch(messageFrom, "ENVELOPE")
          if !envelopeArray.blank?
            envelope = envelopeArray[0].attr["ENVELOPE"]
            #répond avec un mail
            ReponseMailer.new_reponse_email(imap.login, email.email, envelope.message_id, delivery_options).deliver
            imap.increment!(:repCount)

            #lis puis déplace les mails dans IMPORTANT
            imapAccess.store(messageFrom, "+FLAGS", [:Seen])
            imapAccess.copy(messageFrom, "[#{imap.label}]/#{importantFolder}")
            imapAccess.store(messageFrom, "+FLAGS", [:Deleted])
            imap.increment!(:inboxCount)
            imap.increment!(:importantCount)
            imap.increment!(:mailCount)
          end
          puts "[#{Time.now.hour}:#{Time.now.min}] Mail déplacé, réponse envoyée (#{imap.label})".green
          sleep(2)
        end
      end

    #parcourt les spams
    imapAccess.select(spamFolder)
    #déplace les mails dans IMPORTANT et répond aved un mail
    adresses.each do |email|
        imapAccess.search(["FROM", email.email]).each do |messageFrom|
          spamArray = imapAccess.fetch(messageFrom, "ENVELOPE")
          if !spamArray.blank?
            spam = imapAccess.fetch(messageFrom, "ENVELOPE")[0].attr["ENVELOPE"]
            ReponseMailer.new_reponse_email(imap.login ,email.email, spam.message_id, delivery_options).deliver
            imap.increment!(:repCount)
            
            imapAccess.store(messageFrom, "+FLAGS", [:Seen])
            imapAccess.copy(messageFrom, "[#{imap.label}]/#{importantFolder}")
            imapAccess.store(messageFrom, "+FLAGS", [:Deleted])
            imap.increment!(:spamCount)
            imap.increment!(:importantCount)
            imap.increment!(:mailCount)
          end
          puts "Mail déplacé (#{imap.label})".green
          sleep(3)
        end
      end
      rescue => e
      puts "ERREUR ImapJob: #{e} (#{imap.label})\nRETRY...".red
      sleep 5
      imapAccess.disconnect
      perform(id)
      end
  end
end