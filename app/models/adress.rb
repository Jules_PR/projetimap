class Adress < ApplicationRecord
    validates :email, presence: true, uniqueness: true
    validates :password, presence: true
    validates :servsmtp, presence: true
    validates :port, presence: true, numericality: { only_integer: true }
    validates :authentication, presence: true
    validates :starttls, presence: true
    validates :domain, presence: true
end
