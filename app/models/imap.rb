class Imap < ApplicationRecord
    validates :label, presence: true 
    validates :login, presence: true, uniqueness: true
    validates :password, presence: true
    validates :servimap, presence: true
end