# Documentation technique

## IP warm-up

### <ins>Versions utilisées</ins>
>#### Ruby <ins>3.0.1p64</ins>
>----
>#### Ruby on Rails <ins>6.1.4.1</ins>
>----
>#### Redis server <ins>5.0.7</ins>
>---
>#### Sidekiq <ins>6.2.2</ins>
>--------
>#### Mysql <ins>8.0.27</ins>

### <ins>Utilisation en local</ins>
>Installer les dépendances ci-dessus puis adapter le fichier ```/config/database.yml```
>
>Entrer ensuite les commandes suivantes :
>
>```cd projetIMAP```
>
>```bundle install```
>
>```rails server ```
>
>Puis dans un autre terminal :
>
>```redis-server```
>
>Puis dans un autre terminal :
>
>```bundle exec sidekiq```

### <ins>Contexte</ins>
>Cette application web développée avec le framework Ruby on Rails est une application visant à faire "chauffer" des adresses IP/domaines afin d'améliorer leurs réputations aux yeux des differents services de messageries mail.
>#### Problème:
>> Un trop grand nombre de mails envoyés à des utilisateurs cause une mauvaise réputation auprès des services de messagerie (Gmail, Outlook ect...) et donc l'arrivé des mails en spam :
>>
>>![](/img/spam.png)
>#### Solution:
>> Un faux client (adresse réceptrice) est créé dans le but de répondre automatiquement à tous les mails qu'il reçoit, cela créé un trafic que le service de messagerie du client (ici Gmail) va remarquer et par la suite faire augmenter la réputation de l'IP du serveur de l'entreprise. C'est la phase de warmup:
>>
>>![](/img/warmup.png)
>#### Résultat:
>> L'envoie des mails automatiques n'est plus compromis et ils n'arrivent plus dans les spam car l'adresse IP du serveur de l'entreprise est réputée et celui-ci peut donc se permettre d'envoyer un nombre de mail conséquent:
>>
>>![](/img/reputation.png)

### <ins>Fonctionnalités</ins>

>* L'utilisateur peut ajouter et editer des adresses à chauffer via un formulaire
>* L'utilisateur peut ajouter et editer des adresses réceptrices via un formulaire
>* L'application teste automatiquement les adresses et les rend "indisponibles" si le test échoue
>* L'application gère automatiquement l'envoie des mails des adresses à chauffer
>* L'application gère automatiquement l'envoie des réponses des adresses réceptrices
>* Les adresses réceptrices trie les mails et les déplacent en favoris
>* L'application gère automatiquement la croissance du nombre de mails à envoyer par heures/jours
>* L'application récolte et affiche les stats des adresses enregistrées (à chauffer et receptrices)

### <ins>Compatibilité</ins>
>Les adresses receptrices doivent être des adresses :
>* Gmail
>* Outlook
>* Yahoo
>* AOL
>
>Pour ajouter la compatibilité avec un autre service de messagerie, il faut modifier les fichiers suivant en conséquence :
>
>``` app/jobs/imap_job.rb ```
>
>``` app/views/imaps/_form.html.erb ```

### <ins>Fonctionnement</ins>

>* Connecte en SMTP les adresses à chauffer
>* Connecte en IMAP les adresses réceptrices
>* Gère automatiquement les envoies de mails et les réponses entre les adresses à chauffer et les adresses réceptrices
>* Etend les envois de mails, le trie et les réponses sur une heure. Une seule session d'envoie est disponible par heure par adresse à chauffer.

### <ins>Aperçu</ins> :

>#### Page principale :
>>Ici figurent les adresses à chauffer et les adresses réceptrices. Les adresses bleues sont fonctionnelles, les jaunes sont fonctionnelles mais volontairement inactive, les rouges ne sont pas fonctionnelles. Les adresses sont testées à chaque lancement de cette page et leur état est actualisé.
>>
>>![](/img/screen1.PNG)
>
>#### Formulaires :
>> ##### Formulaire d'ajout d'adresse à chauffer :
>>![](/img/screen2.PNG)
>>
>> ##### Formulaire d'ajout d'adresse réceptrice :
>>![](/img/screen3.PNG)
>>
>> ##### Formulaire d'édition d'adresse réceptrice (avec erreur, sans stats) :
>> L'erreur en rouge n'apparait que sur les adresses rouges (non fonctionnelles)
>>![](/img/screen4.PNG)
>> ##### Formulaire d'édition d'adresse réceptrice (sans erreur, avec stats) :
>>![](/img/screen5.PNG)
>
>#### Page de lancement des sessions d'envoies de mail :
>>Dès le lancement de cette page (en cliquant sur une adresse à chauffer), le processus se lance en arrière plan. C'est une session qui dur a peu près une heure et qui va envoyer x nombre de mail aux adresses réceptrices active qui vont par la suite les trier et y répondre. Si l'adresse cliquée est rouge alors la session d'envoie ne se lance pas.
>> 
>>![](/img/screen6.PNG)
>
>#### Terminal lors d'une session d'envoie:
>>![](/img/screen7.PNG)
>>![](/img/screen8.PNG)
>
>#### Terminal lors des tests des adresses
>>Dans la page principale les adresses sont testées toutes les 10 secondes. Lors d'une session, l'adresse en marche est testée tous les 10 mails envoyés et la session s'annule si une erreur survient.
>>![](/img/testAdresses.png)

### <ins>Ce qu'il reste à coder :</ins>
>* Optimisation : si le nombre de mails à envoyer est trop important, la session durera plus d'une heure, ce qui peut causer des problèmes comme une superposition de plusieurs sessions due à la limite dépassée.
>* Bug : nombre illimité de sessions à 23h (du au fait que 23 > 00)
>* Page statistiques rassemblant toutes les statistiques
>* Attribuer les bonnes statistiques aux differentes adresses IP (?)
